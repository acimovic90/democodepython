from setuptools import setup  # type: ignore

setup(name='democode',
      version='0.1',
      description='Demo code for a client',
      url='https://gitlab.com/acimovic90/democode',
      author='Aleksandar Acimovic',
      author_email='aleksandar.acimovic@adgregoit.dk',
      packages=['democode'],
      install_requires=[
          'typing',
          'requests',
      ],
      zip_safe=False)
