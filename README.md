1. This code was developed using Python version 3.8.6
2. Run "pip install ." for installing all the necessary packages from the setup.py 
3. Run WebServer.py, it will start localhost server on port 8080.
4. Afterwards run RequestDispatcher.py.
5. RequestDispatcher cannot seem to run regulary in my Visual Studio Code program, runs fine in the Terminal and in debug mode.
6. Modules included in this project are - json, typing, HttpClient, http.server
7. Mypy has been used for type checking. A decorater method has been giving a  "# type: ignore" comment as it otherwise would show error. Somewhere it is suggested a bug from mypy. 
8. Suggestions for improvement are always welcome.
9. Enjoy :) 
