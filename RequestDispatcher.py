import json
from typing import TypeVar
from HttpClient import HttpClient

T = TypeVar('T')


class RequestDispatcher:
    def __init__(self, http_client: HttpClient):
        self.http_client = http_client

    def logging_delete(self, url: str, callback):
        self.intercept_delete_url = url
        self.intercept_delete_callback = callback

    def post(self, url: str, payload: T) -> str:
        self.payload = json.dumps(payload.__dict__)

        response = self.http_client.post(url, self.payload)
        return response

    def get(self, url: str) -> str:
        response = self.http_client.get(url)
        return response

    def delete(self, url: str) -> str:
        if url == self.intercept_delete_url:
            self.intercept_delete_callback(url)

        response = self.http_client.delete(url)
        return response


class User:
    def __init__(self, id, name):
        self.id = id
        self.name = name


http_client = HttpClient("http://localhost:8080")
dispatcher = RequestDispatcher(http_client)
dispatcher.logging_delete(
    "/users/2", lambda url: print(f"Intercepted deleting {url}"))

response = dispatcher.post("/users", User("1", "John"))
print(response)
response = dispatcher.post("/users", User("2", "Susan"))
print(response)
response = dispatcher.get("/users/1")
print(response)
response = dispatcher.delete("/users/2")
print(response)
