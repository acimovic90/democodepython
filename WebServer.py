import json
from http.server import SimpleHTTPRequestHandler, HTTPServer


class WebServer(SimpleHTTPRequestHandler):
    def _handle_request(self):
        if self.command == "GET":
            self._handle_get()
        elif self.command == "POST":
            self._handle_post()
        elif self.command == "DELETE":
            self._handle_delete()
        else:
            self._handle_other()

    def _handle_get(self):
        self.send_response(200)
        self.end_headers()
        string = bytes('{0} {1}'.format(
            self.command, self.path), encoding='utf-8')
        self.wfile.write(string)

    def _handle_post(self):
        content_length = int(self.headers["Content-Length"])
        json_data = json.loads(self.rfile.read(content_length).decode("utf-8"))
        self.send_response(200)
        self.end_headers()
        string = bytes('{0} {1} {2}'.format(
            self.command, self.path, json_data), encoding='utf-8')
        self.wfile.write(string)

    def _handle_delete(self):
        self.send_response(200)
        self.end_headers()
        string = bytes('{0} {1}'.format(
            self.command, self.path), encoding='utf-8')
        self.wfile.write(string)

    def do_GET(self):
        self._handle_request()

    def do_POST(self):
        self._handle_request()

    def do_DELETE(self):
        self._handle_request()


def run(server_class=HTTPServer, handler_class=WebServer, port=8080):
    server_address = ("", port)
    server = server_class(server_address, handler_class)
    print(f"Starting http server on {port}")
    server.serve_forever()


run()
