import requests
from typing import TypeVar

T = TypeVar('T')


class HttpClient:
    def __init__(self, baseUrl: str):
        self._baseUrl = baseUrl

    def checkPayload(func):
        def isNotEmpty(self, url, payload):
            if not payload:
                print("Payload is empty, try again")
                return
            return func(self, url, payload)
        return isNotEmpty

    @checkPayload  # type: ignore
    def post(self, url: str, payload: T) -> str:
        response = requests.post(self.get_full_url(url), json=payload, headers={
                                 'Content-Type': 'application/json'})
        return response.text

    def get(self, url: str) -> str:
        response = requests.get(self.get_full_url(url), headers={
            'Content-Type': 'text/plain'})
        return response.text

    def delete(self, url: str) -> str:
        response = requests.delete(self.get_full_url(url), headers={
            'Content-Type': 'text/plain'})
        return response.text

    def get_full_url(self, url):
        return f"{self._baseUrl}{url}"
